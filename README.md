# GrowerIQ - Seed to Sale

## What is it

* An app made as challenge for frontend position at GrowerIQ/WillCompute

## Features 

* Upgrade to Angular 10
* Firebase RealtimeDB
* Darkmode
* List filter to filter by Grower or Warehouse employee
* Bitbucket pipeline to run tests and builds for each commit on main branch 

## Demo
* There's an online demo running on Heroku [here](https://grower-iq.herokuapp.com/)
* There's an online demo running on Netlify [here](https://upbeat-stonebraker-cb2ba8.netlify.app/)

## PWA

* After navigating to the demo from your mobile, you can use the option "add to home screen" to run it as a progressive web app.
https://bitbucket.org/johnboxcodes/groweriq-signup/addon/pipelines/home

* Install angular-cli

```npm install -g @angular/cli```

* Clone this repository and install dependencies

```npm i```

## Run

Start the application with:
```npm start``` ou ```ng serve```

After the build, you can access the app in your browser at ```localhost:4200```

## Tests

Run e2e tests:
```npx cypress run```

## Automatic tests and buld for *main* branch

[Here](https://bitbucket.org/johnboxcodes/groweriq-signup/addon/pipelines/home)
