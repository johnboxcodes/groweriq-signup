/// <reference types="Cypress" />

describe("List User", () => {
  it("should redirect to users list when accessing root ", () => {
    cy.visit("/");
    cy.url().should('include', '/users/list')
    cy.get('#user-list--title').should(
      item => expect(item.first()).to.contain('User List')
    );
  });

  it("should contain all columns", () => {
    cy.contains('Name');
    cy.contains('Phone');
    cy.contains('Type of User');
    cy.contains('Detail');
  });

  it("should open create user dialog ", () => {
    cy.visit("/");
    cy.get('#create-user--btn').click();
    cy.get('.create-user-dialog--container').then(
      () => {
        cy.get('#create-user-dialog-title').should(
          title => expect(title.first()).to.contain('Sign up new user')
        )
      }
    )
  });
});
