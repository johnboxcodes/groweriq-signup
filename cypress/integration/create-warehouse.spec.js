describe("Create User", () => {
  beforeEach(() => {
    cy.visit("/");
    cy.wait(1500);
    cy.get("#create-user--btn").click();
  });

  it("should have Grower and Warehouse Employee options", () => {
    cy.get("#create-user-dialog--select").first().click();
    cy.get("mat-option").contains("Grower");
    cy.get("mat-option").contains("Warehouse Employee");
  });

  it("should have all Warehouse employee Form fields when selected", () => {
    cy.get("#create-user-dialog--select").first().click();
    cy.get("mat-option").contains("Grower");
    cy.get("mat-option").contains("Warehouse Employee").click();
    cy.wait(500);
    cy.get("input[name=name]").should("exist");
    cy.get("input[name=address]").should("exist");
    cy.get("input[name=gender]").should("exist");
    cy.get("input[name=phone]").should("exist");
    cy.get("input[name=years_of_experience]").should("exist");
    cy.get("input[name=educational_qualification]").should("exist");
    cy.contains("Inventory management certification").should("exist");
    cy.get("mat-checkbox[name=inventory_management_certification]").should(
      "exist"
    );
  });
  it("should have all Warehouse employee Form fields when selected", () => {
    cy.get("#create-user-dialog--select").first().click();
    cy.get("mat-option").contains("Grower");
    cy.get("mat-option").contains("Warehouse Employee").click();
    cy.wait(500);
    cy.get("input[name=name]").click();
    cy.get("input[name=address]").click();
    cy.get("input[name=gender]").click();
    cy.get("input[name=phone]").click();
    cy.get("input[name=years_of_experience]").click();
    cy.get("input[name=educational_qualification]").click();
    cy.contains("Inventory management certification")
    cy.get("mat-checkbox[name=inventory_management_certification]").click();
    cy.contains('NAME field is required');
    cy.contains('ADDRESS field is required');
    cy.contains('GENDER field is required');
    cy.contains('PHONE field is required');
    cy.contains('YEARS OF EXPERIENCE field is required');
    cy.contains('EDUCATIONAL QUALIFICATION field is required');
  });

  it.only("create warehouse employee user, navigate to detail ", () => {
    const uuid = Cypress._.random(0, 1e6);
    const user = Math.random().toString(36).substring(7);

    cy.get("#create-user-dialog--select").first().click();
    cy.get("mat-option").contains("Grower");
    cy.get("mat-option").contains("Warehouse Employee").click();
    cy.wait(500);

    cy.get("input[name=name]").click();
    cy.get("input[name=name]").type(`${user + uuid + "cypress"}`);
    cy.get("input[name=address]").type("Main St. 100");
    cy.get("input[name=gender]").type("M");
    cy.get("input[name=phone]").type("+1 837-443-233");
    cy.get("input[name=years_of_experience]").type("5");
    cy.get("input[name=educational_qualification]").type("10");
    cy.get("mat-checkbox[name=inventory_management_certification]").click();
    cy.contains("Sign up Warehouse employee").should("exist");
    cy.get("#submit--warehouse").click();
    cy.url().should("include", "detail");
    cy.contains("WAREHOUSE:").should("exist");
    cy.wait(1000);
    cy.contains(`${user + uuid}`.toUpperCase()).should("exist");
  });
});
