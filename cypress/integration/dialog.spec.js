describe("Create User", () => {
  beforeEach(() => {
    cy.visit("/");
    cy.wait(1500)
    cy.get("#create-user--btn").click();
  });
  it("should have a select for the user type", () => {
    cy.get("#create-user--btn");
  });

  it("should have Grower and Warehouse Employee options", () => {
    cy.get("#create-user-dialog--select").first().click();
    cy.get("mat-option").contains("Grower");
    cy.get("mat-option").contains("Warehouse Employee");
  });
  it("should have all Grower Form fields as initial state", () => {
    cy.get("input[name=name]").should("exist");
    cy.get("input[name=address]").should("exist");
    cy.get("input[name=gender]").should("exist");
    cy.get("input[name=phone]").should("exist");
    cy.get("input[name=batches]").should("exist");
    cy.get("input[name=yield]").should("exist");
    cy.get("input[name=green_house_locations]").should("exist");
    cy.get("input[name=educational_qualification]").should("not.exist");
  });
  it("should display validation message for required fields and not show submit button", () => {
    cy.get("input[name=name]").click();
    cy.get("input[name=address]").click();
    cy.get("input[name=gender]").click();
    cy.get("input[name=phone]").click();
    cy.get("input[name=batches]").click();
    cy.get("input[name=yield]").click();
    cy.get("input[name=green_house_locations]").click();
    cy.get("input[name=yield]").click();
    cy.contains("NAME field is required").should("exist");
    cy.contains("ADDRESS field is required").should("exist");
    cy.contains("GENDER field is required").should("exist");
    cy.contains("PHONE field is required").should("exist");
    cy.contains("BATCHES HANDLED field is required").should("exist");
    cy.contains("YIELD ACQUIRED field is required").should("exist");
    cy.contains("Greenhouse location field is required").should("exist");
    cy.contains("Fill all required data to sign up").should("exist");
  });
  it("should show submit button, create user,  navigate to detail ", () => {
    const uuid = Cypress._.random(0, 1e6);
    const user = Math.random().toString(36).substring(7);

    cy.get("input[name=name]").click();
    cy.get("input[name=name]").type(`${user + uuid + 'cypress'}`);
    cy.get("input[name=address]").type("Main St. 100");
    cy.get("input[name=gender]").type("M");
    cy.get("input[name=phone]").type("+1 837-443-233");
    cy.get("input[name=batches]").type("50");
    cy.get("input[name=yield]").type("10");
    cy.get("input[name=green_house_locations]").type(
      "Montevideo{enter}Alberta{enter}"
    );
    cy.contains("Sign up Grower").should("exist");
    cy.get("#submit--grower").click();
    cy.url().should("include", "detail");
    cy.contains("GROWER:").should("exist");
    cy.wait(1000)
    cy.contains(`${user + uuid}`.toUpperCase()).should("exist");
  });
  it("should not accept letters on number fields", () => {

    cy.wait(250)
    cy.get("input[name=batches]").type("aaa");
    cy.get("input[name=batches]").type("123");
    cy.wait(250)
    cy.get("input[name=yield]").type("aaa");
    cy.get("input[name=yield]").type("123");
    cy.wait(250)
    cy.get("input[name=batches]").should('have.value', '123')
    cy.get("input[name=yield]").should('have.value', '123')
  })
  it("should have all Warehouse employee Form fields when selected", () => {
    cy.get("#create-user-dialog--select").first().click();
    cy.get("mat-option").contains("Grower");
    cy.get("mat-option").contains("Warehouse Employee").click();
    cy.wait(500)
    cy.get("input[name=name]").should("exist");
    cy.get("input[name=address]").should("exist");
    cy.get("input[name=gender]").should("exist");
    cy.get("input[name=phone]").should("exist");
    cy.get("input[name=years_of_experience]").should("exist");
    cy.get("input[name=educational_qualification]").should("exist");
    cy.contains('Inventory management certification').should('exist')
    cy.get("mat-checkbox[name=inventory_management_certification]").should("exist");
  });
  it("should have all Warehouse employee Form fields when selected", () => {
    cy.get("#create-user-dialog--select").first().click();
    cy.get("mat-option").contains("Grower");
    cy.get("mat-option").contains("Warehouse Employee").click();
    cy.wait(500)
    cy.get("input[name=name]").click()
    cy.get("input[name=address]").click()
    cy.get("input[name=gender]").click()
    cy.get("input[name=phone]").click()
    cy.get("input[name=years_of_experience]").click()
    cy.get("input[name=educational_qualification]").click()
    cy.contains('Inventory management certification').click()
    cy.get("mat-checkbox[name=inventory_management_certification]").click()
  });
});
