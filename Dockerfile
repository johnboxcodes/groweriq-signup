FROM node:latest as builder
RUN apt-get update && apt-get install -y git

WORKDIR /ng-app
COPY . .
RUN npm i
RUN npx ng build --prod
# RUN $(npm bin)/node json-server.js

FROM nginx:latest
RUN apt-get install bash
COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /ng-app/dist/groweriq-signup /usr/share/nginx/html/
CMD ["nginx", "-g", "daemon off;"]
