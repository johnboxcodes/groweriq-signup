type userType = "grower" | "warehouse"

export interface IContact {
  name: string;
  email: string;
}

export interface IUser {
  uuid: string;
  name: string;
  address: string;
  phone_number: string;
  gender: string;
  years_of_experience: number;
  educational_qualification: string;
  inventory_management_certification: boolean;
  type?: userType;
  batches_handled: number;
  yield_acquired: number;
  greenhouse_locations: string[];
  id: string;
  createdAt?: Date;
  avatar: string;
  users?: IContact[];
  title: string;
  authorId?: number;
  body: string;
}

export type IGrower = Pick<
  IUser,
  | "uuid"
  | "name"
  | "address"
  | "phone_number"
  | "gender"
  | "batches_handled"
  | "yield_acquired"
  | "greenhouse_locations"
  | "type"

>;

export type IWarehouseEmployee = Pick<
  IUser,
  | "uuid"
  | "name"
  | "address"
  | "phone_number"
  | "gender"
  | "years_of_experience"
  | "educational_qualification"
  | "inventory_management_certification"
  | "type"
>;
