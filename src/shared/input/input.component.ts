import { AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormControl } from "@angular/forms";
import { Subscription } from 'rxjs';

type autoCompleteOptions = "on" | "off";

type appearanceType = "legacy" | "standard" | "fill" | "outline" | "none";
// 'none' is not a valid mat-form-field, we pass it as default
// to get a componet without any pre-defined style

type colSizes = "lg" | "md" | "sm";
// use this to set the number of inputs per line;
// eg. a lg size will have one input per line

const COLS = new Map([
  ["lg", "100%"],
  ["md", "40%"],
  ["sm", "25%"],
]);

@Component({
  selector: "app-input",
  templateUrl: "./input.component.html",
  styleUrls: ["./input.component.scss"],
})
export class InputComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() type: string;
  @Input() name: string
  @Input() maxLength: number;
  _colSize = "md";
  @Input() set colSize(value: colSizes) {
    if (!value) return;
    this._colSize = COLS.get(value);
  }
  @Input() inputWidth = "200px";
  @Input() isDisabled = false;
  @Input() suffixIcon: string;
  @Input() text: string;
  @Input() icon: string;
  @Input() isAutoComplete: autoCompleteOptions = "off";
  @Input() showHidePass = false;
  @Input() appearance: appearanceType = "none";
  @Input() label: string = "";
  @Input() control: FormControl;
  subscriptions = new Subscription()
  @ViewChild("input", { static: false }) inputRef: ElementRef;
  constructor() {}
  ngOnDestroy(): void {
    this.subscriptions.unsubscribe()
  }
  ngAfterViewInit(): void {
    if (this.type === 'number') {
     this.subscriptions.add(

      this.control.valueChanges.subscribe(
        value => {
          if ((value !== null)) {
            if ((value.toString().match(/\D+/g !== null))) {
             this.control
              .setValue(value.replace(/\D/g, ''));
            }
          }
        }
      )
    );

    }
  }
  requiredInputs = ["control"];
  ngOnInit(): void {
    this.requiredInputs.forEach((input) => {
      if (!this[input]) {
        throw new TypeError(
          `You must provide ${input} attribute for app-input`
        );
      }
    });

  }
}
