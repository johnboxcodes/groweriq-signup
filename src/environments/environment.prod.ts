export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyA8RkTzYJyYBS8uvGe3LY-9DmwwrCpEilI",
    authDomain: "usersdb-d3275.firebaseapp.com",
    databaseURL: "https://usersdb-d3275.firebaseio.com",
    projectId: "usersdb-d3275",
    storageBucket: "usersdb-d3275.appspot.com",
    messagingSenderId: "422259324281",
    appId: "1:422259324281:web:e65e1bc6a6c5e8926a2ba8",
    measurementId: "G-R9JE151XSJ"
  }
};
