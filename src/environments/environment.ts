// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA8RkTzYJyYBS8uvGe3LY-9DmwwrCpEilI",
    authDomain: "usersdb-d3275.firebaseapp.com",
    databaseURL: "https://usersdb-d3275.firebaseio.com",
    projectId: "usersdb-d3275",
    storageBucket: "usersdb-d3275.appspot.com",
    messagingSenderId: "422259324281",
    appId: "1:422259324281:web:e65e1bc6a6c5e8926a2ba8",
    measurementId: "G-R9JE151XSJ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
