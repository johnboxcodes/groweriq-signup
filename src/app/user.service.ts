import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import {
  IGrower,
  IUser,
  IWarehouseEmployee,
} from "../shared/models/user.model";

@Injectable({
  providedIn: "root",
})
export class UserService {
  public users$: Observable<any[]>;
  usersList = this.db.list<IUser[]>("users");
  constructor(private db: AngularFireDatabase) {
    // get user KEY to retrieve detail
    this.users$ = this.usersList
      .snapshotChanges()
      .pipe(
        map((changes) =>
          changes.map((c) => ({ key: c.payload.key, ...c.payload.val() }))
        )
      );
    // this.users$ = this.db.list<IUser[]>("users").valueChanges();
  }
  create(user: IGrower | IWarehouseEmployee): any {
    return this.db.list("users").push(user);
  }
  getDetails(key) {
    return this.db.list<IUser[]>("users", (ref) =>
      ref.orderByChild("uuid").equalTo(key)
    ).valueChanges().pipe(
      map(item => item[0])
    )
  }
  delete(key: string): Promise<void> {
    return this.usersList.remove(key);
  }

}
