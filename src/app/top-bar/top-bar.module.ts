import { NgModule } from '@angular/core';
import { TopBarComponent } from './top-bar.component';
import { ThemeSwitcherComponent } from './theme-switcher/theme-switcher.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SharedModule } from '../../shared/shared.module';
import { MatButtonToggleModule } from '@angular/material/button-toggle';



@NgModule({
  declarations: [TopBarComponent, ThemeSwitcherComponent],
  imports: [
    MatSlideToggleModule,
    SharedModule,
    MatButtonToggleModule
  ],
  exports: [TopBarComponent]
})
export class TopBarModule { }
