import { Component, OnDestroy, OnInit, Renderer2 } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";

@Component({
  selector: "app-theme-switcher",
  templateUrl: "./theme-switcher.component.html",
  styleUrls: ["./theme-switcher.component.scss"],
})
export class ThemeSwitcherComponent implements OnInit, OnDestroy {
  isChecked = true;
  themeGroup: FormGroup;
  subscriptions = new Subscription();
  constructor(private _renderer: Renderer2, private formBuilder: FormBuilder) {
    this.themeGroup = this.formBuilder.group({
      toggleTheme: false,
    });
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.themeGroup
        .get("toggleTheme")
        .valueChanges.subscribe((toggleValue) => {
          if (toggleValue === true) {
            this._renderer.addClass(document.body, "theme-alternate");
          } else {
            this._renderer.removeClass(document.body, "theme-alternate");
          }
        })
    );
  }
  ngOnDestroy(): void {
    this.subscriptions.unsubscribe()
  }
}
