import { NgModule } from "@angular/core";

import { UserRoutingModule } from "./user-routing.module";
import { SharedModule } from "../../shared/shared.module";
import { CommonModule } from "@angular/common";
import { CreateUserModule } from "./create/create-user.module";

import { MatDialogModule } from "@angular/material/dialog";
import { DetailComponent } from './detail/detail.component';
import { MatCardModule } from '@angular/material/card';
import { DeleteComponent } from './delete/delete.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [DetailComponent, DeleteComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,
    CreateUserModule,
    MatDialogModule,
    MatCardModule,
    MatButtonModule
  ],
})
export class UserModule {}
