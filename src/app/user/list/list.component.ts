import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Subscription } from "rxjs";
import { IUser } from "src/shared/models/user.model";
import { UserService } from "src/app/user.service";
import { CreateUserDialogComponent } from "../create/create-user-dialog/create-user-dialog.component";
import { LoadingService } from "src/app/loading.service";
import { ActivatedRoute, Router } from "@angular/router";
import { DeleteComponent } from "../delete/delete.component";
import { Title } from '@angular/platform-browser';

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.scss"],
})
export class ListComponent implements OnInit, AfterViewInit, OnDestroy {
  visibleColumns = ["name", "phone", "type", "detail", "delete"];
  userData$ = this.userService.users$;
  dataSource = new MatTableDataSource<IUser[]>();
  subscriptions = new Subscription();
  form = new FormGroup({
    filterToggle: new FormControl(),
    filterValue: new FormControl(),
  });
  get filterValue(): FormControl {
    return this.form.get("filterValue") as FormControl;
  }
  get filterToggle(): FormControl {
    return this.form.get("filterToggle") as FormControl;
  }

  @ViewChild(MatSort, { static: false })
  set sort(sortBy: MatSort) {
    this.dataSource.sort = sortBy;
  }
  constructor(
    private titleService: Title,
    private router: Router,
    private userService: UserService,
    private loadingService: LoadingService,
    private dialog: MatDialog
  ) {}

  ngAfterViewInit(): void {
    this.subscriptions.add(
      this.userData$.subscribe(
        (usersData) => {
          this.loadingService.setLoading(true);
          this.dataSource.data = usersData;
        },
        // todo: must implement proper error handling
        (error) => console.log(error),
        () => console.log("completed")
      )
    );
  }
  userTypeFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  };

  applyGrowerFilter() {
    this.dataSource.filter = "grower";
  }
  applyWarehouseFilter() {
    this.dataSource.filter = "warehouse";
  }
  clearFilter() {
    this.dataSource.filter = null;
    this.filterValue.setValue(null);
  }
  redirectTo(key) {
    this.router.navigate([`/users/list/detail/${key}`]);
  }
  createUser() {
    const dialogRef = this.dialog.open(CreateUserDialogComponent, {
      width: "740px",
      height: "620px",
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.userService.create(result);
        this.redirectTo(result.uuid);
      }
    });
  }
  deleteUser(userData) {
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: "400px",
      height: "200px",
      data: { name: userData.name, type: userData.type },
      panelClass: "no-padding-container",
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === "confirm") {
        this.userService.delete(userData.key);
      }
    });

    // this.userService.delete(key)
  }
  ngOnInit(): void {
    this.titleService.setTitle('GrowerIQ - List Users')
    this.dataSource.filterPredicate = (data: any, filter) => {
      return data.type.toLowerCase().includes(filter);
    };

    this.subscriptions.add(
      this.filterToggle.valueChanges.subscribe((filter) =>
        !filter ? this.clearFilter() : null
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
