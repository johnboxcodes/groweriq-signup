import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatTableModule } from "@angular/material/table";
import { MatSortModule } from "@angular/material/sort";
import { MatRadioModule } from "@angular/material/radio";
import { MatCheckboxModule } from "@angular/material/checkbox";

import { ListComponent } from "./list.component";

import { ListRoutingModule } from "./list-routing.module";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from 'src/shared/shared.module';

@NgModule({
  declarations: [ListComponent],
  imports: [
    ListRoutingModule,
    SharedModule,
    CommonModule,
    ReactiveFormsModule,
    MatGridListModule,
    MatRadioModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule
  ],
})
export class ListModule {}
