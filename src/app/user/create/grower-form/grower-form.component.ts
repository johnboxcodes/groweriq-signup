import { COMMA, ENTER } from "@angular/cdk/keycodes";
import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { MatChipInputEvent } from "@angular/material/chips";
import { debounceTime } from "rxjs/operators";
import { IGrower } from "src/shared/models/user.model";

// adding this validator just in case we have a minimum of
// greenhouses to validate
export const minLengthArray = (min: number) => {
  return (c: AbstractControl): { [key: string]: any } => {
    if (c.value.length >= min) return null;

    return { minLength: true };
  };
};

@Component({
  selector: "app-grower-form",
  templateUrl: "./grower-form.component.html",
  styleUrls: ["./grower-form.component.scss"],
})
export class GrowerFormComponent implements OnInit {
  growerForm: FormGroup;
  @Output() statusDetailEvent = new EventEmitter<{
    status: string;
    data: IGrower;
  }>();
  constructor(private formBuilder: FormBuilder) {
    this.growerForm = this.formBuilder.group({
      name: ["", [Validators.required]],
      address: ["", [Validators.required]],
      phone_number: ["", [Validators.required]],
      gender: ["", [Validators.required]],
      batches: [null, [Validators.required]],
      yield_acquired: [null, [Validators.required]],
      greenhouse_locations: [[], [Validators.required, minLengthArray(1)]],
      type: ['grower', [Validators.required]],
    });
  }
  get nameControl(): FormControl {
    return this.growerForm.get("name") as FormControl;
  }
  get addressControl(): FormControl {
    return this.growerForm.get("address") as FormControl;
  }
  get phoneControl(): FormControl {
    return this.growerForm.get("phone_number") as FormControl;
  }
  get genderControl(): FormControl {
    return this.growerForm.get("gender") as FormControl;
  }
  get batchesControl(): FormControl {
    return this.growerForm.get("batches") as FormControl;
  }
  get yieldAcquiredControl(): FormControl {
    return this.growerForm.get("yield_acquired") as FormControl;
  }
  get greenhouseLocationsControl(): FormControl {
    return this.growerForm.get("greenhouse_locations") as FormControl;
  }

  /**
   * chips logic
   */
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  locations: string[] = [];

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || "").trim()) {
      this.locations.push(value.trim());
      this.greenhouseLocationsControl.setValue(this.locations);
    }
    if (input) {
      input.value = "";
    }
  }

  remove(location: string, input): void {
    const index = this.locations.indexOf(location);
    if (index >= 0) {
      this.locations.splice(index, 1);
      this.greenhouseLocationsControl.setValue(this.locations);
    }
    if (input) {
      input.value = "";
    }
  }

  ngOnInit(): void {
    this.growerForm.statusChanges
      .pipe(debounceTime(250))
      .subscribe((formStatus) =>
        this.statusDetailEvent.emit({
          status: formStatus,
          data: this.growerForm.value,
        })
      );
  }
}
