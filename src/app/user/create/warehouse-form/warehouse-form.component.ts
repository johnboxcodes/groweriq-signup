import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { debounceTime } from 'rxjs/operators';
import { IWarehouseEmployee } from "src/shared/models/user.model";

@Component({
  selector: "app-warehouse-form",
  templateUrl: "./warehouse-form.component.html",
  styleUrls: ["./warehouse-form.component.scss"],
})
export class WarehouseFormComponent implements OnInit {
  warehouseForm: FormGroup;
  @Output() statusDetailEvent = new EventEmitter<{
    status: string;
    data: IWarehouseEmployee;
  }>();
  constructor(private formBuilder: FormBuilder) {
    this.warehouseForm = this.formBuilder.group({
      name: ["", [Validators.required]],
      address: ["", [Validators.required]],
      phone_number: ["", [Validators.required]],
      gender: ["", [Validators.required]],
      years_of_experience: [null, [Validators.required]],
      educational_qualification: [null, [Validators.required]],
      inventory_management_certification: [false, [Validators.required]],
      type: ["warehouse", [Validators.required]],
    });
  }
  get nameControl(): FormControl {
    return this.warehouseForm.get("name") as FormControl;
  }
  get addressControl(): FormControl {
    return this.warehouseForm.get("address") as FormControl;
  }
  get phoneControl(): FormControl {
    return this.warehouseForm.get("phone_number") as FormControl;
  }
  get genderControl(): FormControl {
    return this.warehouseForm.get("gender") as FormControl;
  }
  get yearsExperienceControl(): FormControl {
    return this.warehouseForm.get("years_of_experience") as FormControl;
  }
  get qualificationControl(): FormControl {
    return this.warehouseForm.get("educational_qualification") as FormControl;
  }
  get inventoryManagementControl(): FormControl {
    return this.warehouseForm.get(
      "inventory_management_certification"
    ) as FormControl;
  }

  ngOnInit(): void {
    this.warehouseForm.statusChanges
      .pipe(debounceTime(250))
      .subscribe((formStatus) =>
        this.statusDetailEvent.emit({
          status: formStatus,
          data: this.warehouseForm.value,
        })
      );
  }
}
