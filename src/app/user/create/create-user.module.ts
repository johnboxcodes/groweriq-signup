import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CreateUserDialogComponent } from "./create-user-dialog/create-user-dialog.component";
import { InputModule } from "src/app/input/input.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatSelectModule } from "@angular/material/select";
import { MatFormFieldModule } from "@angular/material/form-field";
import { GrowerFormComponent } from "./grower-form/grower-form.component";
import { WarehouseFormComponent } from './warehouse-form/warehouse-form.component';
import {MatChipsModule} from '@angular/material/chips';
import { SharedModule } from 'src/shared/shared.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';

@NgModule({
  declarations: [
    CreateUserDialogComponent,
    WarehouseFormComponent,
    GrowerFormComponent,
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatCheckboxModule,
    SharedModule,
    InputModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatChipsModule
  ],
})
export class CreateUserModule {}
