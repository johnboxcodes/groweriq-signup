import { invalid } from '@angular/compiler/src/render3/view/util';
import { Component, Inject, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BehaviorSubject } from "rxjs";
import { IGrower, IWarehouseEmployee } from "src/shared/models/user.model";
import { v4 as uuidv4 } from "uuid";

interface UserType {
  value: string;
  viewValue: string;
}

const INVALID = "INVALID";
@Component({
  selector: "app-create-user-dialog",
  templateUrl: "./create-user-dialog.component.html",
  styleUrls: ["./create-user-dialog.component.scss"],
})
export class CreateUserDialogComponent implements OnInit {
  userTypes: UserType[] = [
    { value: "grower", viewValue: "Grower" },
    { value: "warehouse", viewValue: "Warehouse Employee" },
  ];
  userTypeForm: FormGroup;
  receiveGrowerData = new BehaviorSubject<{ status: string; data: IGrower }>({
    status: INVALID,
    data: {
      uuid: "",
      address: "",
      batches_handled: 0,
      gender: "",
      greenhouse_locations: [],
      name: "",
      phone_number: "",
      yield_acquired: 0,
    },
  });
  receiveWarehouseData = new BehaviorSubject<{
    status: string;
    data: IWarehouseEmployee;
  }>({status: INVALID, data: {
      uuid: "",
      address: "",
      gender: "",
      name: "",
      phone_number: "",
      educational_qualification: "",
      inventory_management_certification: false,
      years_of_experience: 0,
  }});

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CreateUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public returnData: any
  ) {
    this.userTypeForm = this.formBuilder.group({
      userType: "grower",
    });
  }
  get userType() {
    return this.userTypeForm.get("userType") as FormControl;
  }
  submitGrower() {
    this.receiveGrowerData.value.data.uuid = uuidv4();
    return this.receiveGrowerData.value.data
  }
  submitWarehouse() {
    this.receiveWarehouseData.value.data.uuid = uuidv4();
    return this.receiveWarehouseData.value.data
  }
  ngOnInit(): void {

  }
}
