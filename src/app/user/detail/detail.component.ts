import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";
import { UserService } from "src/app/user.service";
import { Location } from "@angular/common";
import { Title } from '@angular/platform-browser';

@Component({
  selector: "app-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"],
})
export class DetailComponent implements OnInit {
  data$: Observable<any>;
  constructor(
    private titleService: Title,
    private userService: UserService,
    private location: Location,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe(
      (route) => (this.data$ = this.userService.getDetails(route.id))
    );
  }

  goBack() {
    this.location.back();
  }
  ngOnInit(): void {
    this.titleService.setTitle(`GrowerIQ - User detail`)
  }
}
